using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour{
    [SerializeField] private Button btnStart;
    [SerializeField] private Button btnExit;

    // Start is called before the first frame update
    void Start(){
        btnStart.onClick.AddListener(StartGame);
        btnExit.onClick.AddListener(ExitGame);
    }

    private async void StartGame(){
        await PlaySound(btnStart);
        SceneManager.LoadScene("Level 1");
    }

    public async Task PlaySound(Button btn){
        btn.GetComponent<AudioSource>().Play();
        await Task.Delay(1000);
    }

    private async void ExitGame(){
        await PlaySound(btnExit);
        Application.Quit();
    }
}
