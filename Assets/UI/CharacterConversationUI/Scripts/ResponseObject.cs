using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResponseObject
{
    public string ResponseText;

    // public MissionObject ResponseMissionToStart;

    public ConversationOption conversationOption;

    // public void StartMission() {
    //     MissionSystem missionSystem = MissionSystem.instance;
    //     missionSystem.RegisterMission(ResponseMissionToStart);
    // }
    
}
