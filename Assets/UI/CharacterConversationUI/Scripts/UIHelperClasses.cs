using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TextPanel {

    public GameObject TextPanelObject;
    
    public Text TextObject;
}

[System.Serializable]
public class OptionsPanel {
    public GameObject OptionPanelObject;

    public Button BtnOptionA;

    public Button BtnOptionB;
}

[System.Serializable]
public class OtherCharacterUI {
    public Image ImageObject;

    public TextPanel textPanel;
}

[System.Serializable]
public class OptionButton {
    public Button Option;

    public Text OptionText;
}

[System.Serializable]
public class MainCharacterUI {
    public Image ImageObject;

    public TextPanel textPanel;

    public GameObject OptionsPanel;

    public Button ContinueButton;

    public OptionButton OptionA;

    public OptionButton OptionB;
}