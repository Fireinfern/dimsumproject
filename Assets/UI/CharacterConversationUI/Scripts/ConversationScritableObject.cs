using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Conversation", menuName = "ScriptableObjects/Conversations")]
public class ConversationScritableObject : ScriptableObject
{
    public string OtherActorText;

    public CharacterScriptableObject OtherActorCharacter;

    public string MainCharacterText;

    public CharacterScriptableObject MainCharacter;

    public bool bIsResponseAvailable = false;

    public ConversationScritableObject NextConversationObject;

    public ResponseObject ResponseA;

    public ResponseObject ResponseB;

    public ConversationOption ContinueButton;

    public string MissionKeyListening;
}
