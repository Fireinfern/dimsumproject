using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInteractionUISystem : MonoBehaviour
{

    public delegate void BlockMovementInputs();

    public delegate void ActivateMovementInputs();

    public event BlockMovementInputs OnBlockMovementInputs;

    public event ActivateMovementInputs OnActivateMovementInputs;

    public static CharacterInteractionUISystem instance;

    public GameObject ConversationPanel;

    public OtherCharacterUI otherCharacterUI;

    public MainCharacterUI mainCharacterUI;

    [HideInInspector]
    public ConversationScritableObject CurrentConversation;

    void Awake() {
        instance = this;
    }

    public void InitiateResponseConversation() {
        mainCharacterUI.OptionsPanel.SetActive(true);
        mainCharacterUI.OptionA.OptionText.text = CurrentConversation.ResponseA.ResponseText;
        mainCharacterUI.OptionA.Option.onClick.AddListener(CurrentConversation.ResponseA.conversationOption.DoAction);
        mainCharacterUI.OptionB.OptionText.text = CurrentConversation.ResponseB.ResponseText;
        mainCharacterUI.OptionB.Option.onClick.AddListener(CurrentConversation.ResponseB.conversationOption.DoAction);
    }

    public void InitiateNormalConversation() {
        mainCharacterUI.textPanel.TextPanelObject.SetActive(true);
        mainCharacterUI.ContinueButton.onClick.AddListener(CurrentConversation.ContinueButton.DoAction);
    }

    public void InitiateConversation(ConversationScritableObject conversationScritableObject){
        CleanScreen();
        gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        CurrentConversation = conversationScritableObject;
        if(CurrentConversation == null){
            Debug.Log("Null Conversation");
        }
        ConversationPanel.SetActive(true);
        if(OnBlockMovementInputs != null){
            OnBlockMovementInputs();
        }
        mainCharacterUI.ImageObject.sprite = CurrentConversation.MainCharacter.HeadCharacterSprite;
        otherCharacterUI.ImageObject.sprite = CurrentConversation.OtherActorCharacter.HeadCharacterSprite;
        SetTextsForConversation();
        if(conversationScritableObject.bIsResponseAvailable) {
            InitiateResponseConversation();
            return;
        }
        InitiateNormalConversation();
    }

    public void SetTextsForConversation() {
        otherCharacterUI.textPanel.TextObject.text = CurrentConversation.OtherActorText;
        if(CurrentConversation.bIsResponseAvailable){
            return;
        }
        mainCharacterUI.textPanel.TextObject.text = CurrentConversation.MainCharacterText;
    }

    public void ContinueConversation() {
        if(CurrentConversation == null) return;
        if(CurrentConversation.NextConversationObject == null){
            CleanScreen();
            CurrentConversation = null;
            if(OnActivateMovementInputs != null){
                OnActivateMovementInputs();
            }
            return;
        }
        InitiateConversation(CurrentConversation.NextConversationObject);
    }

    public void CleanScreen(){
        mainCharacterUI.ContinueButton.onClick.RemoveAllListeners();
        mainCharacterUI.OptionA.Option.onClick.RemoveAllListeners();
        mainCharacterUI.OptionB.Option.onClick.RemoveAllListeners();
        mainCharacterUI.textPanel.TextPanelObject.SetActive(false);
        mainCharacterUI.OptionsPanel.SetActive(false);
        ConversationPanel.SetActive(false);
    }

}
