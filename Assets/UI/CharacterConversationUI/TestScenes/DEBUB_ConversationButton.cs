using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUB_ConversationButton : MonoBehaviour
{
    public ConversationScritableObject Conversation;

    public void CreateConversation() {
        CharacterInteractionUISystem.instance.InitiateConversation(Conversation);
    }
}
