using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private GameObject interaction;
    private GameObject interactPanel;

    // Start is called before the first frame update
    void Start()
    {
        interactPanel = this.gameObject.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update(){
        InteractionPanel();
    }

    private void InteractionPanel(){
        interaction = player.GetComponent<PlayerMovement>().PlayerInteraction;
        if(interaction != null){
            Vector3 pos = interaction.transform.position;
            pos.y += 1.5f;
            interactPanel.transform.position = pos;
        }
        interactPanel.SetActive(interaction != null);
        
    }
}
