using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : Interactables
{
    [SerializeField] private GameObject targetDoor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected override void DoAction(){
        player.GetComponent<PlayerMovement>().Interacted = true;
        player.transform.position = targetDoor.transform.position;
    }


}
