using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteraction : Interactables
{

    [SerializeField]
    private CharacterScriptableObject CharacterData;

    [SerializeField]
    private ConversationScritableObject ConversationData;

    [SerializeField]
    private List<ConversationScritableObject> CharacterConversations;

    private Dictionary<string, ConversationScritableObject> CharacterConversationDictionary = new Dictionary<string, ConversationScritableObject>(); 

    void Start(){
        foreach (ConversationScritableObject conversation in CharacterConversations)
        {
            CharacterConversationDictionary.Add(conversation.MissionKeyListening, conversation);
        }
    }

    protected override void DoAction()
    {
        CharacterInteractionUISystem interactionUISystem = CharacterInteractionUISystem.instance;
        if(interactionUISystem == null) return;
        interactionUISystem.InitiateConversation(ConversationData);
    }

    public void OnMissionCompleted(MissionObject missionObject) {
        CharacterScriptableObject MissionCharacter = ConversationData.OtherActorCharacter;
        Debug.Log(missionObject.CharactersListeningID.Contains(MissionCharacter.ID));
        if(missionObject.CharactersListeningID.Contains(MissionCharacter.ID)) {
            CheckCharacterListeningMission(missionObject);
            return;
        }
        bIsInteractable = false;
    }

    public void CheckCharacterListeningMission(MissionObject missionObject) {
        ConversationScritableObject conversation = CharacterConversationDictionary[missionObject.MissionName];
        if(conversation == null){
            bIsInteractable = false;
            return;
        }
        // Debug.Log("Conversation Set!");
        bIsInteractable = true;
        ConversationData = conversation;
    }
}
