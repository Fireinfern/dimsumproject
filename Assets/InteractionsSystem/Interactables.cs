using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactables : MonoBehaviour
{
    protected GameObject player;

    [HideInInspector]
    public bool bIsInteractable = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
        GetInteraction();
    }

    void GetInteraction(){
        if (player && Input.GetKeyDown(KeyCode.Space)){
            DoAction();
        }
    }

    virtual protected void DoAction(){
        // Gets Assigned According to interaction with Overrides
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.tag == "Player")
            player = other.gameObject;
    }
    void  OnTriggerExit2D(Collider2D other){
        if(other.gameObject.tag == "Player")
            player = null;
    }
}
