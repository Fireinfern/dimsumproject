using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenDelimiter : MonoBehaviour
{
    private BoxCollider2D coll;
    private Vector3 limit;
    private bool inLimit;

    public Vector3 Limit{get{return limit;}}
    public bool InLimit{get{return inLimit;} set{inLimit = value;}}

    private void Start() {
        coll = gameObject.GetComponent<BoxCollider2D>();
        limit = gameObject.transform.position;
        inLimit = false;
    }

    private void Update() {
        //print(transform.name + ": " + gameObject.transform.position);
        if(!inLimit){
            limit = gameObject.transform.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        inLimit = true;
        limit = other.transform.position;
    }

    private void OnTriggerExit2D(Collider2D other) {
        inLimit = false;
    }
}
