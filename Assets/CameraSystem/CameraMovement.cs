using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] GameObject player;
    private BoxCollider2D coll;

    private ScreenDelimiter limL;
    private ScreenDelimiter limR;

    // Start is called before the first frame update
    void Start(){
        coll = gameObject.GetComponent<BoxCollider2D>();
        limL = transform.GetChild(0).gameObject.GetComponent<ScreenDelimiter>();
        limR = transform.GetChild(1).gameObject.GetComponent<ScreenDelimiter>();
    }

    // Update is called once per frame
    void FixedUpdate(){
        if(player.GetComponent<PlayerMovement>().Interacted){
            //limL.InLimit = false; limL.InLimit = false;
            Vector3 pos = player.transform.position;
            this.transform.position = new Vector3(pos.x, pos.y + 3.5f, -5);
            player.GetComponent<PlayerMovement>().Interacted = false;
        } else {
            float x = Mathf.Clamp(player.transform.position.x, limL.Limit.x + 7.9f, limR.Limit.x - 7.9f);
            float y = player.transform.position.y + 3.5f;
            transform.position = new Vector3(x,y,-5);
        }
    }
}
