using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    [SerializeField]
    private AudioSource MainAudioSource;
    
    [SerializeField]
    private List<DayAudioSet> AudioSets;

    private int CurrentDay;

    private AudioClip CurrentLoop;

    void Awake(){
        instance = this;
    }

    void Start() {
        AtDayStart();
    }

    void Update() {
        if(!MainAudioSource.isPlaying){
            DayAudioSet dayAudioSet = AudioSets[CurrentDay - 1];
            MainAudioSource.clip = CurrentLoop;
            MainAudioSource.Play();
        }
    }

    public void AtDayStart() {
        MissionSystem missionSystem = MissionSystem.instance;
        if(missionSystem == null) return;
        if(missionSystem.CurrentDay >= 5) return;
        CurrentDay = missionSystem.CurrentDay;
        // Debug.Log(CurrentDay);
        DayAudioSet dayAudioSet = AudioSets[missionSystem.CurrentDay - 1];
        MainAudioSource.clip = dayAudioSet.IntroClip;
        CurrentLoop = dayAudioSet.MainLoop;
        MainAudioSource.Play();
    }

    public void SetNightLoop() {
        DayAudioSet dayAudioSet = AudioSets[CurrentDay - 1];
        CurrentLoop = dayAudioSet.Night;
    }
}
