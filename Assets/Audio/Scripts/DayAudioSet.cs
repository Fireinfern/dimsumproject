using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DayAudioSet
{
    public AudioClip IntroClip;

    public AudioClip MainLoop;

    public AudioClip Night;
}
