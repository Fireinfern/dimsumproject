using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour{

    [SerializeField] private float movSpeed;
    private Vector3 playerVelocity;

    private bool bPlayerCanMove = true;

    private Rigidbody2D rigidBody;
    private BoxCollider2D boxCollider;
    private GameObject collideGO = null;
    private bool interacted;

    public GameObject PlayerInteraction{get{return collideGO;}}
    public bool Interacted{get{return interacted;} set{interacted = value;}}

    // Start is called before the first frame update
    void Start()
    {
        CharacterInteractionUISystem characterInteractionUISystem = CharacterInteractionUISystem.instance;
        characterInteractionUISystem.OnBlockMovementInputs += OnBlockMovement;
        characterInteractionUISystem.OnActivateMovementInputs += OnActivateMovement;
        movSpeed = movSpeed == 0.0f ? 5.0f : movSpeed;
        interacted = false;
        playerVelocity = new Vector3(0f,0f,0f);
        rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        boxCollider = this.gameObject.GetComponent<BoxCollider2D>();
    }

    void OnDestroy() {
        CharacterInteractionUISystem characterInteractionUISystem = CharacterInteractionUISystem.instance;
        characterInteractionUISystem.OnBlockMovementInputs -= OnBlockMovement;
        characterInteractionUISystem.OnActivateMovementInputs -= OnActivateMovement;
    }

    // Update is called once per frame
    void Update()
    {
        if(!bPlayerCanMove) return;
        playerVelocity = rigidBody.velocity;
        playerVelocity.x = movSpeed * Input.GetAxisRaw("Horizontal");
        rigidBody.velocity = playerVelocity;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(!other.gameObject.GetComponent<Interactables>().bIsInteractable) return;
        collideGO = other.gameObject;
    }

    private void OnTriggerExit2D(Collider2D other) {
        collideGO = null;
    }

    private void OnBlockMovement() {
        bPlayerCanMove = false;
    }

    private void OnActivateMovement(){
        bPlayerCanMove = true;
    }
}
