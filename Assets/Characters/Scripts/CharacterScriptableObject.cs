using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "ScriptableObjects/Characters")]
public class CharacterScriptableObject : ScriptableObject
{
    public Sprite CharacterSprite;

    public Sprite HeadCharacterSprite;

    public string CharacterName;

    public int ID;

    public List<CharacterScriptableObject> CloseCircle;

    public Color ConversationColor;

    public CharacterCaracteristics Caracteristics;
}
