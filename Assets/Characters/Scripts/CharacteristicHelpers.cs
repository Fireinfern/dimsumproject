using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HairColor {
    BROW,
    BRUNETTE,
    CHESTNUT,
    REDHEAD,
    BLONDE,
    WHITE
}
