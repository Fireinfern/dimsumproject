using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum OptionTypes {
    GAME_OVER,
    FINISH_DAY,
    CONTINUE,
    START_MISSION
}

[System.Serializable]
public class ConversationOption
{
    public OptionTypes type = OptionTypes.CONTINUE;

    public MissionObject MissionToCreate;

    public void DoAction(){
        CharacterInteractionUISystem interactionUISystem = CharacterInteractionUISystem.instance;
        switch (type)
        {
            case OptionTypes.GAME_OVER:
                SceneManager.LoadScene("GameOver");
                break;
            case OptionTypes.FINISH_DAY:
                SceneManager.LoadScene("Level 1");
                break;
            case OptionTypes.CONTINUE:
                interactionUISystem.ContinueConversation();
                break;
            case OptionTypes.START_MISSION:    
                interactionUISystem.ContinueConversation();
                InitializeMission();
                break;
        }
    }

    public void InitializeMission(){
        MissionSystem missionSystem = MissionSystem.instance;
        if(missionSystem == null) return;
        if(MissionToCreate == null) return;
        missionSystem.RegisterMission(MissionToCreate);
        // MissionToCreate.StartFinishedConversation();
    }
}
