using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterCaracteristics
{
    public int age;

    public HairColor hairColor;
}
