using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DEBUG_MissionTextUpdate : MonoBehaviour
{
    [SerializeField]
    private TMP_Text MissionText;

    public void OnMissionRegistered() {
        MissionSystem missionSystem = MissionSystem.instance;
        MissionText.SetText("");
        string FinalText = "";
        if(missionSystem == null) return;
        foreach (var mission in missionSystem.RegisteredMissions)
        {
            FinalText += mission.Value.MissionName + "\n";
            foreach (var step in mission.Value.GetMissionSteps())
            {
                FinalText += "--->" + step.MissionStepText + "\n";
            }
        }
        MissionText.SetText(FinalText);
    }
}
