using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_MissionStepCompletion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    public void ActivateOnStartMission(){
        gameObject.SetActive(true);
    }

    public void FinishStep() {
        MissionStepAssignationComponent missionStepAssignationComponent = GetComponent<MissionStepAssignationComponent>();
        if(missionStepAssignationComponent == null) return;
        missionStepAssignationComponent.FinishStep();
        Destroy(gameObject);
    }
}
