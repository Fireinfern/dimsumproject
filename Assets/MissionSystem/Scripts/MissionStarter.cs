using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionStarter : MonoBehaviour
{
    public TextAsset jsonMission;

    private MissionObject NewMission;

    public void CreateMission(){
        InitializeMission();
        if(NewMission == null) return;
        MissionSystem.instance.RegisterMission(NewMission);
    }

    public void InitializeMission(){
        NewMission = JsonUtility.FromJson<MissionObject>(jsonMission.text);
    }
}
