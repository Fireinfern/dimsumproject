using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Conversation", menuName = "ScriptableObjects/Daily Starter")]
public class DailyStarter : ScriptableObject
{
    public MissionObject DailyMission;
}
