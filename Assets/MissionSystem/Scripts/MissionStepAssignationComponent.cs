using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MissionStepAssignationComponent : MonoBehaviour
{

    public UnityEvent StartMissionStep;

    public string MissionKey;
    public string MissionStepKey;

    void Start() {
        BindToDelegates();
    }

    private void BindToDelegates(){
        MissionSystem missionSystemInstance = MissionSystem.instance;
        Debug.Log("Binded");
        if(missionSystemInstance == null) return;
        missionSystemInstance.OnStepStarted         += StepStarted;
    }

    public void FinishStep() {
        MissionSystem missionSystem = MissionSystem.instance;
        if(missionSystem == null) return;
        MissionObject AssignedMission = missionSystem.RegisteredMissions[MissionKey];
        if(AssignedMission == null) return;
        MissionStepObject MissionStep = AssignedMission.MissionSteps[0];
        if(MissionStep == null && MissionStep.MissionStepName != MissionStepKey) return;
        missionSystem.CompleteStep(MissionKey);
    }

    public void StepStarted(string MissionKey, string MissionStepKey) {
        if(MissionKey != this.MissionKey || MissionStepKey != this.MissionStepKey) return;
        StartMissionStep.Invoke();
    }

}
