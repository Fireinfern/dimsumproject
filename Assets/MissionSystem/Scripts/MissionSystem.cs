using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSystem : MonoBehaviour
{
    // Instance
    public static MissionSystem instance;

    // Delegates
    public delegate void RegisteredMission();

    public delegate void CompletedMission(MissionObject CompletedMission);

    public delegate void CompletedStep(string MissionKey, string MissionStepKey);

    public delegate void StepStarted(string MissionKey, string MissionStepKey);

    // Events
    public event RegisteredMission OnRegisteredMission;

    public event CompletedMission OnCompletedMission;

    public event CompletedStep OnCompletedStep;

    public event StepStarted OnStepStarted;

    [HideInInspector]
    public Dictionary<string, MissionObject> RegisteredMissions = new Dictionary<string, MissionObject>();

    public int CurrentDay = 0;

    void Awake(){
        if(instance == null){
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    public void RegisterMission(MissionObject MissionToBeRegister) {
        RegisteredMissions.Add(MissionToBeRegister.MissionName, MissionToBeRegister);
        if(OnRegisteredMission != null) {
            OnRegisteredMission();
        }
        StartStep(MissionToBeRegister);
    }

    public void CompleteMission(string MissionKey) {
        MissionObject CompletedMission = RegisteredMissions[MissionKey];
        if(CompletedMission == null) return;
        RegisteredMissions.Remove(MissionKey);
        if(OnCompletedMission == null) return;
        OnCompletedMission(CompletedMission);
    }

    public void CompleteStep(string MissionKey){
        if(RegisteredMissions[MissionKey].GetMissionSteps().Count == 0) return;
        MissionStepObject FinishedStep = RegisteredMissions[MissionKey].MissionSteps[0];
        if(FinishedStep == null) return;
        RegisteredMissions[MissionKey].MissionSteps.Remove(FinishedStep);
        if(OnCompletedStep != null) {
            OnCompletedStep(MissionKey, FinishedStep.MissionStepName);
        }
        StartStep(RegisteredMissions[MissionKey]);
    }

    public void StartStep(MissionObject ParentMission){
        if(ParentMission.MissionSteps.Count == 0) {
            CompleteMission(ParentMission.MissionName);
            return;
        }
        MissionStepObject StartedStep = ParentMission.GetMissionSteps()[0];
        if(OnStepStarted == null) return;
        OnStepStarted(ParentMission.MissionName, StartedStep.MissionStepName);
    }
}