using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayChecker : MonoBehaviour
{

    public List<DailyStarter> StartingMissionPerDay;

    [SerializeField]
    private GameObject AudioManager;

    void Start() {
        MissionSystem missionSystem = MissionSystem.instance;
        missionSystem.CurrentDay++;
        missionSystem.OnCompletedMission += OnMissionCompleted;
        CheckDay();
        Instantiate<GameObject>(AudioManager);
    }

    public void CheckDay() {
        MissionSystem missionSystem = MissionSystem.instance;
        if(missionSystem == null) return;
        if(missionSystem.CurrentDay >= 5) return;
        DailyStarter dailyStarter = StartingMissionPerDay[missionSystem.CurrentDay - 1];
        if(dailyStarter == null) return;
        missionSystem.RegisterMission(dailyStarter.DailyMission);
    }

    public void OnMissionCompleted(MissionObject mission){
        //Debug.Log("OnMissionCompleted");
        mission.StartFinishedConversation();
    }
}
