using System.Collections.Generic;

[System.Serializable]
public class MissionObject
{
    public string MissionName;

    public List<MissionStepObject> MissionSteps;

    public ConversationScritableObject ConversationStartedOnFinishedMission;

    public List<int> CharactersListeningID;

    public MissionObject(){}

    ~MissionObject(){}

    public List<MissionStepObject> GetMissionSteps() {
        return MissionSteps;
    }

    public void AddMissionStep(MissionStepObject NewMissionStep){
        MissionSteps.Add(NewMissionStep);
    }

    public void StartFinishedConversation() {
        if(ConversationStartedOnFinishedMission == null) return;
        CharacterInteractionUISystem interactionSystem = CharacterInteractionUISystem.instance;
        interactionSystem.InitiateConversation(ConversationStartedOnFinishedMission);
    }
}
