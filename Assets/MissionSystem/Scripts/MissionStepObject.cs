using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MissionStepObject
{
    public string MissionStepName;

    public string MissionStepText;

    public MissionStepObject(){}

    public MissionStepObject(string MissionStepName, string MissionStepText){
        this.MissionStepName = MissionStepName;
        this.MissionStepText = MissionStepText;
    }

    ~MissionStepObject(){}
}
