using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MissionListener : MonoBehaviour
{
    [SerializeField]
    private UnityEvent RegisterMissionFunction; 

    [SerializeField]
    private UnityEvent<MissionObject> CompletedMissionFunction;

    [SerializeField]
    private UnityEvent<string, string> StepStartFunction;

    [SerializeField]
    private UnityEvent<string, string> StepCompleteFunction;

    void OnDestroy() {
        MissionSystem missionSystemInstance = MissionSystem.instance;
        if(missionSystemInstance == null) return;

        missionSystemInstance.OnRegisteredMission   -= RegisteredMission;
        missionSystemInstance.OnCompletedMission    -= CompletedMission;
        missionSystemInstance.OnStepStarted         -= StepStarted;
        missionSystemInstance.OnCompletedStep       -= CompletedStep;
    }

    void Awake() {
        
    }

    void Start(){
        BindToDelegates();
    }

    private void BindToDelegates(){
        MissionSystem missionSystemInstance = MissionSystem.instance;
        if(missionSystemInstance == null) return;

        missionSystemInstance.OnRegisteredMission   += RegisteredMission;
        missionSystemInstance.OnCompletedMission    += CompletedMission;
        missionSystemInstance.OnStepStarted         += StepStarted;
        missionSystemInstance.OnCompletedStep       += CompletedStep;
    }

    public void RegisteredMission(){
        RegisterMissionFunction.Invoke();
    }
    public void CompletedMission(MissionObject CompletedMission){
        CompletedMissionFunction.Invoke(CompletedMission);
    }
    public void CompletedStep(string MissionKey, string MissionStepKey){
        StepCompleteFunction.Invoke(MissionKey, MissionStepKey);
    }
    public void StepStarted(string MissionKey, string MissionStepKey){
        StepStartFunction.Invoke(MissionKey, MissionStepKey);
    }
}
